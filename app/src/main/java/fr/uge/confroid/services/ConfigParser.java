package fr.uge.confroid.services;

import android.os.Bundle;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;


/**
 * This class gathers all the methods allowing to pass from an object to content of a configuration and vice versa
 */
public class ConfigParser {
    private static final Logger logger = Logger.getLogger(ConfigurationPusher.class.getName());
    private static final Map<Class<?>, Class<?>> TYPE;
    static {
        TYPE = new HashMap<Class<?>, Class<?>>(16);
        TYPE.put(Integer.class, int.class);
        TYPE.put(Byte.class, byte.class);
        TYPE.put(Character.class, char.class);
        TYPE.put(Boolean.class, boolean.class);
        TYPE.put(Double.class, double.class);
        TYPE.put(Float.class, float.class);
        TYPE.put(Long.class, long.class);
        TYPE.put(Short.class, short.class);
        TYPE.put(Void.class, void.class);
    }
    private static HashMap<String, Field> allFields(Class<?> aClass) throws ClassNotFoundException{
        HashMap<String, Field> fields = new HashMap<String, Field>();

        while (aClass != null) {
            for (Field f : aClass.getDeclaredFields()) {
                fields.put(f.getName(), f);
            }
            aClass = aClass.getSuperclass();
        }
        return fields;
    }

    private static HashMap<String, Object> BundleToMap(Bundle bundle){
        HashMap<String, Object> map = new HashMap<>();
        Set<String> keys = bundle.keySet();

        for (String key: keys) {
            Object obj = bundle.get(key);
            if (obj instanceof Bundle){
                map.put(key, BundleToMap((Bundle)bundle.get(key)));
            }
            else if(obj.getClass().isArray()){
                map.put(key, ArrayToMap(bundle.get(key)));
            }
            else {
                map.put(key, classRepresentation(bundle.get(key)));
            }
        }
        map.put("_class", bundle.getClass().getName());
        return map;
    }

    private static HashMap<String, Object> ArrayToMap(Object array){
        int length = Array.getLength(array);
        HashMap<String, Object> representation = new HashMap<>();
        for (int i = 0; i < length; i++) {
            representation.put(Integer.toString(i), classRepresentation(Array.get(array, i)));
        }
        representation.put("_class", array.getClass().getName());
        return representation;
    }

    static boolean isPrimitiveType(Object source) {
        return TYPE.containsKey(source.getClass());
    }
    /**
     * Transform a class to the content of a {@link Config}
     * @param object the object to parse
     * @return An Object representing the content of an {@link Config}
     */
    static Object classRepresentation(Object object){
        HashMap<String, Object> representation = new HashMap<>();


        Class<?> aClass = object.getClass();

        if (object instanceof Bundle){
            logger.info("INSTANCE isBundle : " + aClass);
            return BundleToMap((Bundle)object);
        }
        else if(object instanceof CharSequence){
            logger.info("INSTANCE isCharSequence : " + aClass);
            return object;
        }
        else if(aClass.isArray()){
            logger.info("INSTANCE isArray : " + aClass);
            return ArrayToMap(object);
        }
        else if(aClass.isPrimitive()){
            logger.info("INSTANCE isPrimitive : " + aClass);
            return object.toString();
        }
        try{
            logger.info("INSTANCE aClass : " + aClass);
            while (aClass != null) {

                for (Field f : aClass.getDeclaredFields()) {
                    //logger.info("Field : " + f);
                    int modifier = f.getModifiers();
                    if (!Modifier.isStatic(modifier) && !Modifier.isTransient(modifier)){
                        logger.info("INSTANCE IF : " + f.get(object));

                        f.setAccessible(true);
                        if (f.getType().equals(Bundle.class)){
                            logger.info("INSTANCE TRY BUNDLE : " + f.get(object));
                            representation.put(f.getName(), BundleToMap((Bundle)f.get(object)));
                        }
                        else if(f.getType().isArray()) {
                            logger.info("INSTANCE TRY ISARRAY : " + f.get(object));
                            representation.put(f.getName(), ArrayToMap(f.get(object)));
                        }
                        else {
                            logger.info("INSTANCE TRY ELSE : " + f.get(object));
                            representation.put(f.getName(), classRepresentation(f.get(object)));
                        }
                    }
                    else{
                        logger.info("INSTANCE TRY ELSE DU ELSE");

                    }
                }
                aClass = aClass.getSuperclass();

            }
        }
        catch (Exception ex){
            logger.info("INSTANCE Exception on sort");
        }
        logger.info("INSTANCE C'est la sortie ");

        if (representation.isEmpty()){
            logger.info("INSTANCE SORT SORT");
            return object;
        }
        representation.put("_class", object.getClass().getName());


        logger.info("INSTANCE Je ressors ! ");
        return representation;

    }


}
