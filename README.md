## About the project
The Confroid project consists in creating an Android application allowing the centralized management of configurations. Any application installed on the device will have the option of entrusting the storage of its configuration to Confroid rather than performing this task itself.

## Description
<p align="center">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Android_robot.svg/170px-Android_robot.svg.png" alt="Logo" height="350">
</p>
The user will be able to modify the configuration of an application using Confroid. The configurations will be stored in a transactional mode with a history of versions.

### Developed with
* [Java](https://docs.oracle.com/en/java/javase/13/docs/api/index.html)
* [Spring-Boot](https://spring.io/projects/spring-boot)
* [Angular](https://angular.io/)

### Architecture
This application is an Android app build with Java/Kotlin.

## Getting Started

### Requirements
For building and running the application you need:

* [Java](https://www.java.com/fr/)
* [Android](https://developer.android.com/studio?gclid=Cj0KCQjw9YWDBhDyARIsADt6sGYTI6s35WSUvSwPlWxB0JV0z-GcLHy808dJeTBNEFC_2qgxViutem4aAprrEALw_wcB&gclsrc=aw.ds)
* [Kotlin](https://kotlinlang.org/)

### Clone the Repository
As usual, you get started by cloning the project to your local machine:
```
git clone https://gitlab.com/ZacharieSouioued/projet-android.git
```

### Use application
You can then import each folder (Confroid and ExampleApp) on your IDE and launch Confroid and the ExampleApp 

## Developers
* [BANGOURA Thierno](https://gitlab.com/BThierno)
* [ERIALC Jean-Manuel](https://gitlab.com/jeanmanuel.erialc)
* [NGOUNOU Franck](https://gitlab.com/fngouou)
* [RANARIVELO Brian](https://gitlab.com/)
* [SOUIOUED Zacharie](https://gitlab.com/ZacharieSouioued)
