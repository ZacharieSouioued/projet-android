package fr.uge.confroid.viewers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.viewers.ui.AppConfigs;

public class ConfigModification extends AppCompatActivity {
    Logger logger = Logger.getLogger(AppConfigs.class.getName());

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_modification);
        ConstraintLayout constraintLayout  = findViewById(R.id.ConfigModification);
        HashMap<String,Object> content =  (HashMap<String, Object>)getIntent().getSerializableExtra("listOfContent");
        String intent_title = getIntent().getStringExtra("title");
        String intent_tag = getIntent().getStringExtra("tag");
        String intent_version = getIntent().getStringExtra("version");
        int size = (content == null) ? 0 : content.size()-1;

        final TextView[] myTextViews = new TextView[size]; // create an empty array;
        final TextView[] myEditTextViews = new EditText[size]; // create an empty array;


        TextView title = findViewById(R.id.name_config_label);
        title.setText(intent_title);
        TextView version = findViewById(R.id.version_state_label);
        version.setText(intent_version);
        TextView tag = findViewById(R.id.tag_state_label);
        if(intent_tag == null){
            tag.setText("None");
        }else{
            tag.setText(intent_tag);
        }

        Button btn = findViewById(R.id.button_valider);
        int j = 0;
        btn.setOnClickListener((v) -> {
            HashMap<String,Object> map = getContentValue(myTextViews, myEditTextViews);
            logger.info("Serializable : " + map);
            Intent intent = new Intent();
            intent.putExtra("name",intent_title);
            intent.putExtra("tag",intent_tag);
            intent.putExtra("version",intent_version);
            intent.putExtra("content",map);
            Toast.makeText(getApplicationContext(), "Config updated", Toast.LENGTH_SHORT).show();
            setResult(0,intent);
            finish();
        });

        ConstraintSet constraintSet = new ConstraintSet();

        int i = 0;
        int previousSize = 515;

        if(content != null){
            content.remove("_class");
            for(Map.Entry e : content.entrySet()){
                ConstraintLayout.LayoutParams layoutParamsText = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams. MATCH_PARENT , ConstraintLayout.LayoutParams. MATCH_PARENT) ;
                ConstraintLayout.LayoutParams layoutParamsEdit = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT , ConstraintLayout.LayoutParams.WRAP_CONTENT) ;


                logger.info("DIRECTION : " + layoutParamsEdit.getLayoutDirection());
                layoutParamsText.setMargins( 100 , previousSize, 0 , 0 );
                layoutParamsEdit.setMargins( 350 , 0, 100 , 0 );

                logger.info("previousSize ---------------------------------");

                logger.info("previousSize  : " + i);
                logger.info("previousSize  : " + e.getValue().toString().length());

                logger.info("previousSize ---------------------------------");


                // create a new textview
                TextView rowTextView = new TextView(this);
                EditText editText = new EditText(this);

                rowTextView.setId(View.generateViewId());
                rowTextView.setText(e.getKey() + " : ");
                rowTextView.setTextSize(16);
                editText.setId(View.generateViewId());
                editText.setPadding(0, 0 , 120, 20);
                editText.setText(e.getValue().toString());
                editText.setTextSize(16);


                constraintSet.clone(constraintLayout);

                constraintLayout.addView(rowTextView, layoutParamsText);
                constraintLayout.addView(editText,layoutParamsEdit);
                constraintSet.clone(constraintLayout);

                constraintSet.connect(editText.getId(),ConstraintSet.LEFT,rowTextView.getId(),ConstraintSet.LEFT,150);
                constraintSet.connect(editText.getId(),ConstraintSet.TOP,constraintLayout.getId(),ConstraintSet.TOP,previousSize);
                constraintSet.applyTo(constraintLayout);

                // add the textview to the linearlayout
                //constraintLayout.addView(rowTextView);

                // save a reference to the textview for later
                myTextViews[i] = rowTextView;
                myEditTextViews[i] = editText;
                previousSize += ((i+1)  * (90 + e.getValue().toString().length()));
                i+=1;
            }
        }

    }

    private HashMap<String,Object> getContentValue ( TextView[] myTextViews, TextView[] myEditTextViews){
        HashMap<String,Object> map = new HashMap<>();
        for(int j = 0; j < myEditTextViews.length;j++){
            map.put(myTextViews[j].getText().toString(),myEditTextViews[j].getText().toString());
        }
        return map;
    }
}