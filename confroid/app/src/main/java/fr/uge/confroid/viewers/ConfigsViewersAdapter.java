package fr.uge.confroid.viewers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.services.ConfigParser;
import fr.uge.confroid.services.Config;
import fr.uge.confroid.viewers.ui.AppConfigs;

public class ConfigsViewersAdapter extends RecyclerView.Adapter<ConfigsViewersAdapter.ViewHolder> {

    private static Logger logger = Logger.getLogger(ConfigsViewersAdapter.class.getName());

    //private ArrayList<String>configsName;
    //private ArrayList<Config> configs;

    //private HashMap<String, Config> configs;
    //private ArrayList<Map.Entry<String, Config>> configs;
    private final int nbVersions;
    private String configName;
    private ArrayList<Config> configs;
    private AppConfigs appConfigs;
    private Context context;
    private RecyclerViewClickInterface recyclerViewClickInterface;

    public ConfigsViewersAdapter(RecyclerViewClickInterface recyclerViewClickInterface, Context context, AppConfigs appConfigs, String configName, int nbVersions) {
        //this.configs = new ArrayList<Config>(configs.values());
        this.configName = configName;
        this.nbVersions = nbVersions;
        configs = new ArrayList<>(nbVersions);
        this.appConfigs = appConfigs;
        this.context = context;
        this.recyclerViewClickInterface = recyclerViewClickInterface;
    }
    /*
    private ConfigsViewersAdapter(String configName, ArrayList<Config>> configs){
        //this.configs = new ArrayList<Config>(configs.values());
        this.configs = configs;
        logger.info("configsName " + configs);
    }

    public static ConfigsViewersAdapter create(ArrayList<String> configsName){
        //HashMap<String, Config> configs = new HashMap<>();
        ArrayList<Map.Entry<String, Config>> configs = new ArrayList<>();
        for (int i = 0; i < configsName.size(); i++){
            configs.add(new Map.Entry(configsName.get(i), null));
        }
        for (String configName: configsName) {
            configs.put(configName, null);
        }
        return new ConfigsViewersAdapter(configs);
    }
    */

    @NonNull
    @Override
    public ConfigsViewersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder view = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.config_view, parent, false));

        return view;
    }

    @Override
    public int getItemCount() {
        logger.info("itemCount : " + nbVersions);
        return nbVersions;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.update(configName, position);



        /*holder.itemView.setOnClickListener((v) -> {
            *//*String content = appConfigs.getFileContent(holder.config_name.getText().toString());
            Toast.makeText(context, "content : " + content, Toast.LENGTH_LONG).show();

            HashMap<String, String> elem = appConfigs.getContentConfig(holder.config_name.getText().toString());
            HashMap<String, String> config = appConfigs.getElemConfig(holder.config_name.getText().toString());

            Intent intent = new Intent(context, ConfigModification.class);
            logger.info("ConfigInfo : " + config.get("version"));
            logger.info("ConfigInfo : " + config.get("tag"));

            intent.putExtra("title", configName);
            intent.putExtra("listOfContent", elem);
            intent.putExtra("tag", config.get("tag"));
            intent.putExtra("version", config.get("version"));

            ((Activity) context).startActivityForResult(intent, Activity.RESULT_OK);*//*
        });

        holder.itemView.setOnLongClickListener((v) -> {
            new AlertDialog.Builder(context)
                    .setTitle("Voulez-vous supprimer cet élément ?")
                    .setPositiveButton("Oui", null)
                    .setNegativeButton("Non", null)
                    .show();

            return true;
        });*/
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView config_name;
        ConstraintLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            config_name = itemView.findViewById(R.id.config_name);
            mainLayout = itemView.findViewById(R.id.ConfigView);
            logger.info("Cariople la cariole");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewClickInterface.onItemClick(getAdapterPosition(),config_name.getText().toString());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    recyclerViewClickInterface.onLongItemClick(getAdapterPosition(),config_name.getText().toString());
                    return false;
                }
            });
        }

        //void update(Map.Entry<String, ArrayList<Config>> entry){
        void update(String title, int position) {
            //name = entry.getKey();
            //config = entry.getValue();

            //this.title.setText(title.split("_")[0]);
            logger.info("title " + title);
           /* String[] title_split = title.split("/.");
            title = title_split[title_split.length - 1];*/
            logger.info("DENTITION : " + title + " _ " + position);
            logger.info("DENTITION : " + config_name);

            logger.info("DENTITION : " + this.config_name);
            this.config_name.setText(title + '_' + position);
        }
    }

}
