package fr.uge.confroid.services;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Represents a App configuration saved by the Confroid Service
 *
 */
public class Config {

    private String name;

    @Nullable
    private String tag;
    private long version;
    private Object content;

    private static Logger logger = Logger.getLogger(Config.class.getName());

    public Config(String name, @Nullable String tag, long version, Object content) {
        this.name = name;
        this.tag = tag;
        this.version = version;
        this.content = content;
    }

    public void removeTag(){
        tag = null;
    }

    public String getName() {
        return name;
    }

    @Nullable
    public String getTag() {
        return tag;
    }

    public boolean hasTag(){
        return tag != null;
    }

    public long getVersion() {
        return version;
    }

    public Object getContent() {
        return content;
    }

    /**
     *
     * @return a copy of the current config the next version number
     */
    public Config copyWithNextVersion(){
        return new Config(name, tag, (version + 1), cloneContent(content));
    }

    private static Object cloneContent(Object object) {
        try{
            Class<?> objectClass = object.getClass();

            if(object instanceof HashMap || object instanceof LinkedTreeMap) {
                logger.info("cloneContent HashMap Instance "+ object);

                HashMap<String, Object> tmp = (HashMap<String, Object>) object;
                HashMap<String, Object> map = new HashMap<>();

                for (Map.Entry<String, Object> entry : tmp.entrySet()) {
                    map.put(entry.getKey(), cloneContent(entry.getValue()));
                }
                return map;
            }
            if(objectClass.isPrimitive() || objectClass.equals(String.class)
                    || objectClass.getSuperclass().equals(Number.class)
                    || objectClass.equals(Boolean.class)){
                logger.info("Primitive Instance : " + object);
                return object;
            }
            logger.info("Other Instance");

            Object clone = object.getClass().newInstance();
            for (Field field : object.getClass().getDeclaredFields()) {
                int modifier = field.getModifiers();
                field.setAccessible(true);
                if(Modifier.isFinal(modifier) || Modifier.isTransient(modifier) || Modifier.isStatic(modifier) || field.get(object) == null){
                    continue;
                }
                if(field.getType().isPrimitive() || field.getType().equals(String.class)
                        || field.getType().getSuperclass().equals(Number.class)
                        || field.getType().equals(Boolean.class)){
                    field.set(clone, field.get(object));
                }else{
                    Object fieldObj = field.get(object);
                    logger.info(fieldObj.getClass().getName());
                    if(fieldObj == object){
                        field.set(clone, object);
                    }
                    else{
                        field.set(clone, cloneContent(fieldObj));
                    }
                }
            }
            return clone;
        }catch(Exception e){
            logger.warning("unable to clone config " + e);
            return null;
        }
    }

    @Override
    public String toString() {
        return "Config{" +
                "name='" + name + '\'' +
                ", tag='" + tag + '\'' +
                ", version=" + version +
                ", content=" + content +
                '}';
    }

    public void export(Context context){
        logger.info("XVERSION : " + version);
        String configName = name + '_' + version;
        String configJson = new GsonBuilder().setPrettyPrinting().create().toJson(this);
        logger.info("export " + configName + "\n" + configJson);

        try(FileOutputStream outputStream = context.openFileOutput(configName, Context.MODE_PRIVATE)) {
            //outputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            outputStream.write(configJson.getBytes(Charset.forName("UTF-8")));
        } catch (FileNotFoundException e) {
                    ; // file should be created
        } catch (IOException e) {
            logger.warning("export: IO exception " + e.getMessage());
        }
    }

    public static Config load(Context context, String name){
        Config object = null;
        Gson gson = new Gson();

        try(FileInputStream inputStream = context.openFileInput(name)){
            InputStreamReader reader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            StringBuilder strb = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null){
                strb.append(line).append('\n');
            }
            object = fromJson(strb.toString());
            return object;
        }
        catch (FileNotFoundException exception){
            logger.warning("no config find with name " + name);
        }
        catch (IOException e) {
                logger.warning("load: an IO exception occurs");
                return null;
        }
        return null;
    }


    public static HashMap<String, String> jsonToMap(String t) throws JSONException {
        logger.info("JE SUIS T : " + t);

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();
        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);
        }



        return map;
    }

    private static Config fromJson(String field){
        String name="";
        String tag="";
        Long version=0L;
        HashMap<String, Object> map = new HashMap<>();
        try{
            JSONObject jField = new JSONObject(field);
            name = jField.get("name").toString();
            tag = jField.get("tag").toString();
            //version = Long.getLong(jField.get("version").toString());

            String content = jField.getJSONObject("content").toString();
            JSONObject jObject = new JSONObject(content);

            Iterator<?> keys = jObject.keys();


            while( keys.hasNext() ){

                String key = (String)keys.next();

                String value = jObject.getString(key);

                map.put(key, value);
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return new Config(name,tag,version,map);
    }
    /*private static HashMap<String, String> getContentConfig(String fileName){
        Gson gson = new Gson();
        Config object = null;
        File file = new File(getFilesDir(), fileName);
        if (!file.exists()){
            return  null;
        }
        try {
            FileInputStream fin = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            object = gson.fromJson(sb.toString(), Config.class);
            HashMap<String, String> map = jsonToMap(object.getContent().toString());

            reader.close();
            fin.close();

            return map;

        }catch (IOException | JSONException e) {
            return null;
        }

    }*/
}
