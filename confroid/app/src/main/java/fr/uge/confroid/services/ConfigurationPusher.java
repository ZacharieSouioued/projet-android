package fr.uge.confroid.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import fr.uge.confroid.receivers.TokenDispenser;

import static fr.uge.confroid.services.ConfigParser.classRepresentation;

/**
 * The {@link ConfigurationPusher} is a service which allows you to save a Bundle, primitive or a {@link java.io.Serializable} class
 * To save a configuration you must call the service by an {@link Intent} which contains put a Bundle in Extras
 * The Bundle should have these keys:
 *      name : configuration Name (e.g. fr.uge.calculator)
 *      content : content of configuration (Bundle, primitive or {@link java.io.Serializable} class)
 *      token : generated token by {@link TokenDispenser}
 *      tag (optional) : the configuration tag
 */
public class ConfigurationPusher extends Service {
    public static final String SAVE_CONFIGURATION = ConfigurationPusher.class.getName() + ".saveConfig";
    public static final String UPDATE_CONFIGURATION = ConfigurationPusher.class.getName() + ".updateConfig";
    public static final String SELECT_CONFIGURATION = ConfigurationPusher.class.getName() + ".selectConfig";
    public static final String GET_ALL_CONFIGURATIONS = ConfigurationPusher.class.getName() + ".getAllConfig";

    private static final String PREFERENCES_NAME = "fr.uge.confroid.receivers.TokenDispenser";
    private static final Logger logger = Logger.getLogger(ConfigurationPusher.class.getName());

    private final HashMap<String, ArrayList<Config>> data = new HashMap<>();
    private final HashMap<String, Config> dataToChange = new HashMap<>();

    private SharedPreferences allTokens;

    private void addConfiguration(Bundle bundle){

        String name = bundle.getString("name");
        String tag = bundle.getString("tag");
        String token = bundle.getString("token");
        Object content = bundle.get("content");


        if (content == null && tag != null){ // config with empty content  special case in the subject of the project
            content = new HashMap<String, Object>();
        }
        Object objectContent = content;
        new Handler().post(() -> {
            if (objectContent == null){
                logger.info("empty content and no tag");
                Toast.makeText(getApplicationContext(), "empty content and no tag", Toast.LENGTH_SHORT).show();
                return;
            }
            if (token == null){
                logger.info("no token");
                Toast.makeText(getApplicationContext(), "no token", Toast.LENGTH_SHORT).show();
                return;
            }
            if (name == null){
                logger.info("no name given for configuration");
                Toast.makeText(getApplicationContext(), "no name given for configuration", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!token.equals(allTokens.getString(name.split("/")[0], null))){

                Toast.makeText(getApplicationContext(), "wrong token", Toast.LENGTH_SHORT).show();
                return;
            }

            if (name.contains("/")){ // replace an existing config

                String properties[] = name.split("/"); // [name , key to replace]
                if (properties.length != 2){
                    Toast.makeText(getApplicationContext(), "wrong name", Toast.LENGTH_SHORT).show();
                    return;
                }
                ArrayList<Config> allAppConfig = data.get(properties[0]);
                if (allAppConfig == null || allAppConfig.isEmpty()){
                    Toast.makeText(getApplicationContext(), "no previous config", Toast.LENGTH_SHORT).show();
                    return;
                }
                Config lastConfig = allAppConfig.get(allAppConfig.size() - 1);
                Config newConfig = lastConfig.copyWithNextVersion();
                HashMap<String, Object> newConfigContent = (HashMap<String, Object>)newConfig.getContent();
                if (newConfigContent == null || !newConfigContent.containsKey(properties[1])){
                    Toast.makeText(getApplicationContext(), "no key founded", Toast.LENGTH_SHORT).show();
                    return;
                }
                newConfigContent.put(properties[1], classRepresentation(objectContent));
                lastConfig.removeTag();
                allAppConfig.add(newConfig);
                Context context = getApplicationContext();

                newConfig.export(context);

                logger.info(allAppConfig.toString());
            }
            else { // addNewConfig
                logger.info("INFO Standard CREATION ! ");

                Toast.makeText(getApplicationContext(), "Config saved", Toast.LENGTH_SHORT).show();


                logger.info("Probleme 1");

                Object representation = classRepresentation(objectContent);
                logger.info("INFO : " + new Bundle().getString("_class"));
                ArrayList<Config> appConfigs = data.get(name);
                if (appConfigs == null) {
                    appConfigs = new ArrayList<>();
                }
                data.put(name, appConfigs);
                logger.info("INFO NAME : " + name);
                logger.info("INFO NAME : " + data.get(name));

                Config newConfig = new Config(name, tag, appConfigs.size(), representation);
                if (newConfig.hasTag()) {
                    for (Config cfg : appConfigs) { // remove older tag
                        if (cfg.hasTag() && cfg.getTag().equals(newConfig.getTag())) {
                            cfg.removeTag();
                        }
                    }
                }
                appConfigs.add(newConfig);
                Context context = getApplicationContext();
                newConfig.export(context);
                //Config.load(context, newConfig.getName() + '_' + newConfig.getVersion());

                logger.info(appConfigs.toString());

            }
        });
    }

    public void replaceContent(Bundle bundle){

        Object objectContent = bundle.get("content");
        String name = bundle.getString("name");

        String properties[] = name.split("/"); // [name , key to replace]
        if (properties.length != 2){
            Toast.makeText(getApplicationContext(), "wrong name", Toast.LENGTH_SHORT).show();
            return;
        }

        /*dataToChange.put(properties[0],Config.load(getApplicationContext(),properties[0]));
        allAppConfig.add(dataToChange.get(properties[0]));*/
        ArrayList<Config> allAppConfig = data.get(properties[0].split("_")[0]);

        if (allAppConfig == null || allAppConfig.isEmpty()){
            Toast.makeText(getApplicationContext(), "no previous config", Toast.LENGTH_SHORT).show();
            return;
        }

        Config lastConfig = allAppConfig.get(allAppConfig.size() - 1);
        Config newConfig = lastConfig.copyWithNextVersion();
        HashMap<String, Object> newConfigContent = (HashMap<String, Object>)newConfig.getContent();


        if (newConfigContent == null || !newConfigContent.containsKey(properties[1])){
            Toast.makeText(getApplicationContext(), "no key founded", Toast.LENGTH_SHORT).show();
            return;
        }

        logger.info("PROPERTIES : " + properties[1]);
        newConfigContent.put(properties[1], classRepresentation(objectContent));
        //newConfigContent = (HashMap<String, Object>) classRepresentation(objectContent);

        lastConfig.removeTag();
        allAppConfig.add(newConfig);
        newConfig.export(getApplicationContext());

    }

    public HashMap<String, ArrayList<Config>> getData() {
        return data;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemented (since we do not use RPC methods)");
    }

    private void getAllConfig(Bundle bundle){
        new Handler().post(() -> {
            String name = bundle.getString("name");
            String properties[] = name.split("/"); // [name , key to replace]

            logger.info("All Config : " + data);
            ArrayList<Config> allAppConfig = data.get(properties[0].split("_")[0]);

            Bundle newBundle = new Bundle();
            newBundle.putInt("nbConfig",(allAppConfig!=null)?allAppConfig.size():0);

            Intent intent = new Intent( "com.example.exampleappusingconfroid.ExampleCallSaveService.receiveConfig");
            intent.setPackage("com.example.exampleappusingconfroid");
            intent.putExtras(newBundle);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        });
    }

    private void selectConfig(Bundle bundle){
        new Handler().post(() -> {
            String name = bundle.getString("name");
            String properties[] = name.split("/"); // [name , key to replace]
            int choiceConfig = bundle.getInt("nameConfig");

            logger.info("NAME SALUT LES MECS !!");


            ArrayList<Config> newConf = new ArrayList<>();
            newConf.add(data.get(properties[0].split("_")[0]).get(choiceConfig));
            Object x = ConfigParser.classRepresentation(newConf.get(0).getContent());
            Class<?> aClass = x.getClass();
            Bundle newBundle = new Bundle();
            newBundle.putString("version", bundle.getString("version"));
            newBundle.putString("tag", bundle.getString("tag"));

            if (x instanceof Bundle || aClass.isArray()){
                logger.info("ICI " + aClass);
                newBundle.putSerializable("config",(HashMap<String, Object>)x);
            }
            else if(aClass.isPrimitive()){
                newBundle.putSerializable("config",x.toString());
            }
            else{
                newBundle.putSerializable("config",(HashMap<String, Object>)x);
            }


            Intent intent = new Intent( "com.example.exampleappusingconfroid.ExampleCallSaveService.receiveMyConfig");
            intent.setPackage("com.example.exampleappusingconfroid");
            intent.putExtras(newBundle);
            Toast.makeText(this, "Configuration loaded", Toast.LENGTH_LONG).show();

            startService(intent);

           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }*/
        });
    }

    @Override
    public void onCreate() {

        super.onCreate();
        Context context = getApplicationContext();
        allTokens = context.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        context.getApplicationContext().getFilesDir().list();
        logger.info("all tokens " + allTokens.getAll());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle bundle = intent.getExtras();
        if (bundle == null){
            return START_NOT_STICKY;
        }
        String action = intent.getAction();
        if (SAVE_CONFIGURATION.equals(action)){
            addConfiguration(bundle);
        }
        else if (UPDATE_CONFIGURATION.equals(action)){
            replaceContent(bundle);
        }
        else if (GET_ALL_CONFIGURATIONS.equals(action)){
            getAllConfig(bundle);
        }
        else if (SELECT_CONFIGURATION.equals(action)){
            logger.info("");
            selectConfig(bundle);
        }
        //TODO add other actions
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.info("Destruction ConfigurationPusher service");
        Toast.makeText(this, "Destruction ConfigurationPusher service", Toast.LENGTH_LONG).show();

        // TODO save configurations in file
    }
}
