package fr.uge.confroid.receivers;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.Random;
import java.util.logging.Logger;

import fr.uge.confroid.services.ConfigurationPusher;

/**
 * This service allows a user to retrieve a token allowing him to authenticate himself when sending his configuration to the {@link ConfigurationPusher}  service
 */
public class TokenDispenser extends Service {
    // action of the intent sent by the service containing the application token
    public static final String RETURN_TOKEN_INTENT = TokenDispenser.class.getName() + ".returnToken";

    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvxyz@!";
    private static final Logger logger = Logger.getLogger(TokenDispenser.class.getName());
    private static final String PREFERENCES_NAME = "fr.uge.confroid.receivers.TokenDispenser";

    private final Random r = new Random();
    private SharedPreferences allTokens;

    @Override
    public void onCreate() {
        super.onCreate();
        logger.info("create the TokenDispenser service");
        allTokens = getApplicationContext().getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE); // gets all tokens created in previous executions
        //allTokens.edit().clear().apply(); //TODO remove after testing
        logger.info("all tokens available" + allTokens.getAll());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String generateRandomString(){
        int length = r.nextInt(6) + 15; // between 15 and 20
        StringBuilder stringBuilder = new StringBuilder(length);

        for (int i = 0; i < stringBuilder.capacity(); i++){
            stringBuilder.append(CHARACTERS.charAt(r.nextInt(CHARACTERS.length())));
        }
        return stringBuilder.toString();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        logger.info("TokenDispenser onStartCommand");
        logger.info(" all tokens " + allTokens.getAll());
        if (intent == null){
            return START_NOT_STICKY;
        }
        new Handler().post(() -> {
            String name = intent.getStringExtra("name");
            String receiver = intent.getStringExtra("receiver");

            if (name == null || receiver == null){
                return;
            }
            if (allTokens.getString(name, null) != null){ // token already generated
                logger.info("token already  generated");
                logger.info("token is : " + allTokens.getString(name, null));
                return;
            }
            // we generate a token for this new Application
            String token = generateRandomString();
            logger.info("token generated" + token);
            allTokens.edit().putString(name, token).apply();
            logger.info("receiver " + receiver);
            Intent returnIntent = new Intent(receiver);
            returnIntent.setPackage(receiver.substring(0, receiver.lastIndexOf('.')));
            logger.info("RECEIVER : " + receiver.substring(0, receiver.lastIndexOf('.')));
            returnIntent.putExtra("token", token);
            startService(returnIntent);
        });
        return START_STICKY;
    }
}
