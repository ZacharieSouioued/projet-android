package fr.uge.confroid.viewers;

public interface RecyclerViewClickInterface {
    void onItemClick(int i,String config_name);
    void onLongItemClick(int i,String config_name);
}
