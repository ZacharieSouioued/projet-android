package fr.uge.confroid.viewers.ui.configs.view;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ConfigViewDefaultAdapter extends RecyclerView.Adapter<ConfigViewDefaultAdapter.ViewHolder>{

    private Object configToDisplay;

    @NonNull
    @Override
    public ConfigViewDefaultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ConfigViewDefaultAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return configToDisplay.getClass().getDeclaredFields().length;
    }

    class ViewHolder extends  RecyclerView.ViewHolder{
        // TODO use addView
        // LinearLayout myLayout = findViewById(R.id.main);
        //
        //Button myButton = new Button(this);
        //myButton.setLayoutParams(new LinearLayout.LayoutParams(
        //                                     LinearLayout.LayoutParams.MATCH_PARENT,
        //                                     LinearLayout.LayoutParams.MATCH_PARENT));
        //
        //myLayout.addView(myButton);
        String fieldName;
        View value;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

        }
    }
}
