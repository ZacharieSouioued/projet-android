package fr.uge.confroid.viewers;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.Arrays;
import java.util.logging.Logger;

import fr.uge.confroid.R;

public class ConfigsViewers extends AppCompatActivity {
    private static Logger logger = Logger.getLogger(ConfigsViewers.class.getName());
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configs_viewers);

        //HashMap<String, ArrayList<Config>> configs =  (HashMap<String, ArrayList<Config>>)savedInstanceState.get("configs");
        //getFilesDir().list();
        //logger.info(savedInstanceState.get("configs") + " " + configs);
        logger.info("FILES " + Arrays.toString(getFilesDir().list()));
        //ConfigsViewersAdapter adapter = new ConfigsViewersAdapter(getFilesDir().list());
        /*
        RecyclerView recyclerView = findViewById(R.id.all_configs);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, adapter.getItemCount(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

         */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}