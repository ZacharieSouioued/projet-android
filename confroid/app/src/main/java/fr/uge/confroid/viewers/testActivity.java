package fr.uge.confroid.viewers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.viewers.ui.AppConfigs;
import fr.uge.confroid.viewers.ui.HomeFragment;

public class testActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static final Logger logger = Logger.getLogger(testActivity.class.getName());

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private HashMap<String, ArrayList<String>> allAppsconfigs = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Fragment a = new HomeFragment();
        getFragmentManager().beginTransaction().replace(R.id.fragmentView, a).commit();
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        String[] configs = getFilesDir().list();
        Menu menu = navigationView.getMenu();
        Random random = new Random();

        for (int i = 0; i < configs.length; i++){

            //menu.add(R.id.confroid_configs_nav, i, i, configs[i].substring(configs[i].lastIndexOf(".") + 1));
            String configName = configs[i].substring(0, configs[i].lastIndexOf("_"));
            ArrayList<String> appConfigs = allAppsconfigs.computeIfAbsent(configName, s -> new ArrayList<>());
            if (appConfigs.isEmpty()){
                MenuItem item = menu.add(R.id.confroid_configs_nav, i, i, configName);
            }
            appConfigs.add(configs[i]);

        }
        menu.setGroupVisible(R.id.confroid_configs_nav, true);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //navigationView.getMenu().fin
        drawerLayout.openDrawer(GravityCompat.START);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.test, menu);

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, drawerLayout)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "Appuie sur item onOptionsItemSelected", Toast.LENGTH_LONG).show();
        logger.info("appuie sur item onOptionsItemSelected" + item.getTitle().toString());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Random random = new Random();
        //Toast.makeText(this, "Appuie sur item", Toast.LENGTH_LONG).show();
        //logger.info("appuie sur item " + item.getTitle().toString());
        //findViewById(R.id.text_home).setBackgroundColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
        AppConfigs f = new AppConfigs();
        Bundle bundle = new Bundle();
        //bundle.putString("data", "data " + item.getItemId());
        bundle.putString("title", item.getTitle().toString());
        bundle.putSerializable("data", allAppsconfigs.get(item.getTitle()));
        logger.info("titlexxx : " + item.getTitle().toString());
        logger.info("dataxxx : "+ allAppsconfigs.get(item.getTitle()));
        logger.info("dataxxx : "+ allAppsconfigs);

        f.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.fragmentView, f).commit();
        drawerLayout.closeDrawer(navigationView);
        return true;
    }
}