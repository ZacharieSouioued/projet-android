package fr.uge.confroid.viewers.ui.configs.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.services.Config;
import fr.uge.confroid.services.ConfigParser;

public class ConfigView extends AppCompatActivity {

    private static Logger logger = Logger.getLogger(ConfigView.class.getName());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_view);
        String configString = null;
        if ((configString = getIntent().getStringExtra("config")) == null){
            //Toast.makeText(this, "no config given", Toast.LENGTH_LONG);
            logger.info("no config given");
            finish();
        }
        logger.info("configString " +  configString);
        Gson gson = new Gson();
        Config cfg = gson.fromJson(configString, Config.class);
        logger.info("cfg: " + cfg.toString() + " content:" + cfg.getContent() + " " + cfg.getContent().getClass().getName());
        //Object obj = ConfigParser.getObject(cfg.getContent());

        TextView configName = findViewById(R.id.config_view_name);
        TextView versionName = findViewById(R.id.tag_name);
        TextView tagName = findViewById(R.id.version_name);

        configName.setText(cfg.getName());
        versionName.setText("Version: " + cfg.getVersion());
        tagName.setText("Tag: " + cfg.getTag());

        RecyclerView recyclerView = findViewById(R.id.config_view_fields);
        //ConfigViewTmpAdapter adapter = ConfigViewTmpAdapter.create(obj);
        ConfigViewTmpAdapter adapter = ConfigViewTmpAdapter.create(cfg);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        logger.info("size: " + adapter.getItemCount());

    }
}