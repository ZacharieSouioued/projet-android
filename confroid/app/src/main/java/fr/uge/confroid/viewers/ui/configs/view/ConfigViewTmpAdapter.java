package fr.uge.confroid.viewers.ui.configs.view;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import fr.uge.confroid.R;
import fr.uge.confroid.services.Config;

public class ConfigViewTmpAdapter extends RecyclerView.Adapter<ConfigViewTmpAdapter.ViewHolder>{

    private ArrayList<Map.Entry<String, Object>> configToDisplay;

    private ConfigViewTmpAdapter(ArrayList<Map.Entry<String, Object>> configToDisplay) {
        this.configToDisplay = configToDisplay;
    }

    public static ConfigViewTmpAdapter create(Object objectToRepresent){
    //public static ConfigViewTmpAdapter create(Config cfg){
        //Object content = cfg.getContent();
        //Class<?> objClass = cfg.getContent().getClass();
        Class<?> objClass = objectToRepresent.getClass();
        ArrayList<Map.Entry<String, Object>> fields = new ArrayList<>();
        if (objClass.isArray()){
            for (int i = 0; i < Array.getLength(objectToRepresent); i++) {
                fields.add(new AbstractMap.SimpleEntry<>("" + i, Array.get(objectToRepresent, i)));
            }
        }
        else if (objClass.equals(Bundle.class)){
            Bundle bundle = (Bundle) objectToRepresent;
            for (String key: bundle.keySet()) {
                fields.add(new AbstractMap.SimpleEntry<>(key, bundle.get(key)));
            }
        }
        else if (objClass.equals(List.class)){
            List<Object> lst = (List<Object>) objectToRepresent;
            for (int i = 0; i < lst.size(); i++) {
                fields.add(new AbstractMap.SimpleEntry<>("" + i, lst.get(i)));
            }
        }
        else if (objClass.equals(Map.class)){
            Map<Object, Object> map = (Map<Object, Object>)objectToRepresent;
            for (Map.Entry<Object, Object> entry: map.entrySet()) {
                fields.add(new AbstractMap.SimpleEntry<>(entry.getKey().toString(), entry.getValue()));
            }
        }
        else{
            fields.add(new AbstractMap.SimpleEntry<>("config", objectToRepresent));
        }

        return new ConfigViewTmpAdapter(fields);
    }

    @NonNull
    @Override
    public ConfigViewTmpAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConfigViewTmpAdapter.ViewHolder view = new ConfigViewTmpAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.config_view, parent, false));
        view.itemView.setOnClickListener((v) -> {
            ;
        });
        return view;
    }

    @Override
    public void onBindViewHolder(@NonNull ConfigViewTmpAdapter.ViewHolder holder, int position) {
        holder.update(configToDisplay.get(position));
    }

    @Override
    public int getItemCount() {
        return configToDisplay.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder{
        // TODO use addView
        // LinearLayout myLayout = findViewById(R.id.main);
        //
        //Button myButton = new Button(this);
        //myButton.setLayoutParams(new LinearLayout.LayoutParams(
        //                                     LinearLayout.LayoutParams.MATCH_PARENT,
        //                                     LinearLayout.LayoutParams.MATCH_PARENT));
        //
        //myLayout.addView(myButton);
        TextView fieldName;
        View value;
        ConstraintLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.config_view_layout);
            fieldName = itemView.findViewById(R.id.config_name);
        }

        void update(Map.Entry<String, Object> field) {
            fieldName.setText(field.getKey());
            Object obj = field.getValue();
            Class<?> objClass = obj.getClass();


            if (objClass.isArray()){
                Toast.makeText(layout.getContext(), "array", Toast.LENGTH_SHORT).show();
                TextView textView = new TextView(layout.getContext());
                textView.setId(View.generateViewId());
                textView.setText(Arrays.deepToString((Object[]) obj));
                textView.setLayoutParams( new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                ConstraintSet contraintSet = new ConstraintSet();
                contraintSet.connect(textView.getId(), ConstraintSet.LEFT ,R.id.guideline_config_view, ConstraintSet.RIGHT, 0);
                layout.addView(textView);
                contraintSet.applyTo(layout);
            }
            else if (obj instanceof Number){
                Toast.makeText(layout.getContext(), "Number", Toast.LENGTH_SHORT).show();
                EditText editText = new EditText(layout.getContext());
                editText.setId(View.generateViewId());
                editText.setHint(obj.toString());
                //layout.addView(editText);guideline_config_view
                //editText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                layout.addView(editText);
                ConstraintSet contraintSet = new ConstraintSet();
                contraintSet.clone(layout);
                contraintSet.connect(editText.getId(), ConstraintSet.LEFT , R.id.guideline_config_view, ConstraintSet.RIGHT, 0);
                contraintSet.connect(editText.getId(), ConstraintSet.TOP , R.id.config_view_layout, ConstraintSet.TOP, 0);
                contraintSet.connect(editText.getId(), ConstraintSet.BOTTOM , R.id.config_view_layout, ConstraintSet.BOTTOM, 0);
                //contraintSet.connect(editText.getId(), ConstraintSet.BOTTOM , fieldName.getId(), ConstraintSet.BOTTOM, 10);
                ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
                params.horizontalBias = 0.5f;
                editText.setLayoutParams(params);
                editText.setGravity(Gravity.CENTER);
                contraintSet.applyTo(layout);
//                contraintSet.applyToLayoutParams(editText.getId(), (ConstraintLayout.LayoutParams) editText.getLayoutParams());
                //Toast.makeText(layout.getContext(), editText.getLayoutParams().toString(), Toast.LENGTH_LONG).show();
            }
            else if(obj instanceof Boolean){
                Toast.makeText(layout.getContext(), "boolean", Toast.LENGTH_SHORT).show();
                CheckBox checkBoxTrue = new CheckBox(layout.getContext());
                checkBoxTrue.setId(View.generateViewId());
                checkBoxTrue.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
                ConstraintSet contraintSetTrue = new ConstraintSet();
                contraintSetTrue.connect(checkBoxTrue.getId(), ConstraintSet.LEFT ,R.id.guideline_config_view, ConstraintSet.RIGHT, 0);
                contraintSetTrue.applyTo(layout);
                checkBoxTrue.setText("True");
                CheckBox checkBoxFalse = new CheckBox(layout.getContext());
                checkBoxFalse.setId(View.generateViewId());
                checkBoxFalse.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
                ConstraintSet constraintSetFalse = new ConstraintSet();
                constraintSetFalse.connect(checkBoxFalse.getId(), ConstraintSet.LEFT ,checkBoxTrue.getId(), ConstraintSet.RIGHT, 0);
                constraintSetFalse.applyTo(layout);
                checkBoxFalse.setText("False");
                layout.addView(checkBoxTrue);
                layout.addView(checkBoxFalse);
            }
            else if (obj instanceof Map){
                Toast.makeText(layout.getContext(), "map", Toast.LENGTH_SHORT).show();
            }
            else if (obj instanceof List){
                Toast.makeText(layout.getContext(), "list", Toast.LENGTH_SHORT).show();
                if (true){
                    return;
                }
                RecyclerView recyclerView = new RecyclerView(layout.getContext());
                recyclerView.setAdapter(ConfigViewTmpAdapter.create(obj));
                recyclerView.setLayoutManager(new GridLayoutManager(layout.getContext(), 1, LinearLayoutManager.VERTICAL, false));
            }
            else{
                Toast.makeText(layout.getContext(), "default", Toast.LENGTH_SHORT).show();
                TextView defaultText = new TextView(layout.getContext());
                defaultText.setId(View.generateViewId());
                defaultText.setText(obj.toString());
                defaultText.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
                ConstraintSet contraintSet = new ConstraintSet();
                contraintSet.connect(defaultText.getId(), ConstraintSet.LEFT ,R.id.guideline_config_view, ConstraintSet.RIGHT, 0);
                contraintSet.applyTo(layout);
                layout.addView(defaultText);
            }

        }
    }
}
