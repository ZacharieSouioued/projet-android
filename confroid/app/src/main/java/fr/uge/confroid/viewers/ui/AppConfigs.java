package fr.uge.confroid.viewers.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.services.Config;
import fr.uge.confroid.services.ConfigurationPusher;
import fr.uge.confroid.viewers.ConfigModification;
import fr.uge.confroid.viewers.ConfigsViewers;
import fr.uge.confroid.viewers.ConfigsViewersAdapter;
import fr.uge.confroid.viewers.RecyclerViewClickInterface;
import fr.uge.confroid.viewers.ui.home.HomeFragment;

public class AppConfigs extends Fragment implements RecyclerViewClickInterface {
    Logger logger = Logger.getLogger(AppConfigs.class.getName());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.configs_viewers, container, false);


        Bundle bundle = getArguments();
        logger.info("INFORMATION " + bundle);
        if (bundle != null){
            String title =  bundle.getString("title").replace("\n", "");
            logger.info("INFORMATION TITLE : " + title);

            ArrayList<String> data = (ArrayList<String>)bundle.getSerializable("data");
            //((TextView)root.findViewById(R.id.config_title)).setText(title);
            ConfigsViewersAdapter adapter = new ConfigsViewersAdapter(this,getActivity(),this, title, data.size());
            RecyclerView recyclerView = root.findViewById(R.id.all_configs);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new GridLayoutManager(root.getContext(), adapter.getItemCount(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.addItemDecoration(new DividerItemDecoration(this.getContext(), DividerItemDecoration.VERTICAL));
        }
        return root;
    }

    public String getFileContent(String fileName){
        File file = new File(getContext().getFilesDir(), fileName);
        if (!file.exists()){
            return  null;
        }
        try {
        FileInputStream fin = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        fin.close();
        return sb.toString();
        } catch (IOException e) {
            return null;
        }
    }

    public  HashMap<String, Object> getContentConfig(String fileName){
        Gson gson = new Gson();
        HashMap<String, Object> map = null;
        Config object = null;
        File file = new File(getContext().getFilesDir(), fileName);
        logger.info("FILE : " + file.toString());
        if (!file.exists()){
            return  null;
        }
        try {
            FileInputStream fin = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            object = gson.fromJson(sb.toString(), Config.class);
            JSONObject jso = new JSONObject(sb.toString());

            if(object.getContent().getClass().isPrimitive() || (object.getContent() instanceof String)){
                String simpleField = "{\"content\":\""+jso.get("content")+"\"}";
                map = simpleJsonToMap(simpleField);
            }
            else{
                map = jsonToMap(jso.getJSONObject("content").toString());
                return map;
            }
            reader.close();
            fin.close();
            return map;

        }catch(IOException | JSONException e){
            return null;
        }
    }

    public  HashMap<String, String> getElemConfig(String fileName){
        Gson gson = new Gson();
        Config object = null;
        File file = new File(getContext().getFilesDir(), fileName);
        if (!file.exists()){
            return  null;
        }
        try {

            FileInputStream fin = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            object = gson.fromJson(sb.toString(), Config.class);

            HashMap<String, String> map = new HashMap<>();
            map.put("tag",object.getTag());
            map.put("version",Long.toString(object.getVersion()));

            reader.close();
            fin.close();

            return map;

        }catch (IOException e) {
            return null;
        }

    }

    public  HashMap<String, Object> jsonToMap(String t) throws JSONException {

        HashMap<String, Object> map = new HashMap<>();


        JSONObject jObject = new JSONObject(t);

        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        return map;
    }

    public  HashMap<String, Object> simpleJsonToMap(String t) throws JSONException {

        HashMap<String, Object> map = new HashMap<>();
        JSONObject jObject = new JSONObject(t);

        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        return map;
    }


    @Override
    public void onItemClick(int i,String name) {

        String content = this.getFileContent(name);
        Toast.makeText(getContext(), "content : " + content, Toast.LENGTH_LONG).show();


        HashMap<String, Object> elem = this.getContentConfig(name);
        HashMap<String, String> config = this.getElemConfig(name);

        Intent intent = new Intent(getContext(), ConfigModification.class);
        logger.info("ConfigInfo : " + config.get("version"));
        logger.info("ConfigInfo : " + config.get("tag"));

        intent.putExtra("title", name);
        intent.putExtra("listOfContent", elem);
        intent.putExtra("tag", config.get("tag"));
        intent.putExtra("version", config.get("version"));

        startActivityForResult(intent, 0);
    }

    @Override
    public void onLongItemClick(int i,String name) {
        File file = new File(getContext().getFilesDir(), name);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Do you want to delete this config ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(file.delete()){
                            Toast.makeText(getContext(), "Config file deleted !", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(getContext(), "Deleted failed !", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("No",null)
                .show();

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bundle bundle = new Bundle();
        bundle.putString("name",data.getStringExtra("name")+"/0");
        bundle.putString("tag",data.getStringExtra("tag"));
        bundle.putString("version",data.getStringExtra("version"));
        bundle.putSerializable("content",data.getSerializableExtra("content"));


        ConfigurationPusher c = new ConfigurationPusher();
        Intent save = new Intent(getContext(),ConfigurationPusher.class);
        save.setAction("fr.uge.confroid.services.ConfigurationPusher.updateConfig");
        save.setPackage("fr.uge.confroid");
        save.putExtras(bundle);
        getActivity().startService(save);
        //c.replaceContent(getContext(),bundle);

        /*Intent save = new Intent("fr.uge.confroid.services.ConfigurationPusher.updateConfig");
        save.setPackage("fr.uge.confroid");
        save.putExtras(bundle);
        getActivity().startService(save);*/

        /*if(resultCode == Activity.RESULT_OK){
            addConfiguration
        }*/

    }

}
