package fr.uge.confroid.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.receivers.TokenDispenser;
import fr.uge.confroid.viewers.ui.configs.ConfigsViewers;
import fr.uge.confroid.viewers.testActivity;

/**
 * The Confroid main foreground service
 * This foreground service is launched for each Confroid actions and parse these actions
 */
public class ConfroidMainApp extends Service {
    private static final String CHANNEL_ID = "ConfroidServiceChannel";
    private static final Logger logger = Logger.getLogger(ConfroidMainApp.class.getName());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Confroid Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }

        Notification notification =
                new Notification.Builder(this, CHANNEL_ID)
                        .setContentTitle("Confroid")
                        .setContentText("Confroid actif")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setCategory(Notification.CATEGORY_SERVICE)
                        //.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(), 0))
                        .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, testActivity.class), 0))
                        .build();

        startForeground(1, notification);

        Intent actionToachieve = null;
        String action = intent.getAction();
        if (action == null){
            logger.info("no action");
            return START_NOT_STICKY;
        }
        Bundle parameters = intent.getExtras();
        switch (action){
            case "fr.uge.confroid.services.ConfigurationPusher.saveConfig":
                actionToachieve = new Intent(this, ConfigurationPusher.class);
                actionToachieve.setAction(ConfigurationPusher.SAVE_CONFIGURATION);
                actionToachieve.putExtras(parameters);
                startService(actionToachieve);
                break;

            case "fr.uge.confroid.services.ConfigurationPusher.getAllConfig":
                actionToachieve = new Intent(this, ConfigurationPusher.class);
                actionToachieve.setAction(ConfigurationPusher.GET_ALL_CONFIGURATIONS);
                actionToachieve.putExtras(parameters);
                startService(actionToachieve);
                break;

            case "fr.uge.confroid.services.ConfigurationPusher.selectConfig":
                actionToachieve = new Intent(this, ConfigurationPusher.class);
                actionToachieve.setAction(ConfigurationPusher.SELECT_CONFIGURATION);
                actionToachieve.putExtras(parameters);
                startService(actionToachieve);
                break;

            case "fr.uge.confroid.receivers.TokenDispenser":
                actionToachieve = new Intent(this, TokenDispenser.class);
                actionToachieve.putExtras(parameters);
                startService(actionToachieve);
                break;
            default:    // normally impossible because of intent-filter
                logger.info("unknown action " + action);
        }
        return START_NOT_STICKY;
    }
}
