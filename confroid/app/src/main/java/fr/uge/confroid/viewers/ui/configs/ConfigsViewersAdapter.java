package fr.uge.confroid.viewers.ui.configs;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.logging.Logger;

import fr.uge.confroid.R;
import fr.uge.confroid.services.Config;
import fr.uge.confroid.viewers.ui.AppConfigs;
import fr.uge.confroid.viewers.ui.configs.view.ConfigView;

public class ConfigsViewersAdapter extends RecyclerView.Adapter<ConfigsViewersAdapter.ViewHolder>{

    private static Logger logger = Logger.getLogger(ConfigsViewersAdapter.class.getName());

    //private ArrayList<String>configsName;
    //private ArrayList<Config> configs;

    //private HashMap<String, Config> configs;
    //private ArrayList<Map.Entry<String, Config>> configs;
    private final int nbVersions;
    private String configName;
    private ArrayList<Config> configs;
    private AppConfigs appConfigs;

    public ConfigsViewersAdapter(AppConfigs appConfigs, String configName, int nbVersions){
        //this.configs = new ArrayList<Config>(configs.values());
        this.configName = configName;
        this.nbVersions = nbVersions;
        configs = new ArrayList<>(nbVersions);
        this.appConfigs = appConfigs;
    }
    /*
    private ConfigsViewersAdapter(String configName, ArrayList<Config>> configs){
        //this.configs = new ArrayList<Config>(configs.values());
        this.configs = configs;
        logger.info("configsName " + configs);
    }

    public static ConfigsViewersAdapter create(ArrayList<String> configsName){
        //HashMap<String, Config> configs = new HashMap<>();
        ArrayList<Map.Entry<String, Config>> configs = new ArrayList<>();
        for (int i = 0; i < configsName.size(); i++){
            configs.add(new Map.Entry(configsName.get(i), null));
        }
        for (String configName: configsName) {
            configs.put(configName, null);
        }
        return new ConfigsViewersAdapter(configs);
    }
    */

    @NonNull
    @Override
    public ConfigsViewersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.config_view, parent, false));
        ViewHolder view =  new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.config_viewer, parent, false));
        view.itemView.setOnClickListener((v) -> {
            String content = appConfigs.getFileContent(view.title.getText().toString());
            //Toast.makeText(parent.getContext(), "content : " + content, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(parent.getContext(), ConfigView.class);
            intent.putExtra("config", content);
            parent.getContext().startActivity(intent);
        });
        return view;
    }

    @Override
    public int getItemCount() {
        return nbVersions;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.update(configName, position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private String name;
        private ArrayList<Config> configs;
        private TextView title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.config_name);
        }

        //void update(Map.Entry<String, ArrayList<Config>> entry){
        void update(String title, int position){
            //name = entry.getKey();
            //config = entry.getValue();

            //this.title.setText(title.split("_")[0]);
            logger.info("title " + title);
            this.title.setText(title + '_' + position);
        }
    }
}
