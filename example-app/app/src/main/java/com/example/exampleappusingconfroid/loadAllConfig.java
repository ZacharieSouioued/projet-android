package com.example.exampleappusingconfroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.logging.Logger;

public class loadAllConfig extends AppCompatActivity {
    private static Logger logger = Logger.getLogger(ExampleCallSaveService.class.getName());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_all_config);
        int nbOfConfig = getIntent().getIntExtra("nbOfConfig", 0);
        String configName = getIntent().getStringExtra("nameConfig");

        ConstraintLayout constraintLayout  = findViewById(R.id.ConfigSelection);

        final TextView[] myTextViews = new TextView[nbOfConfig]; // create an empty array;
        final TextView[] mySelectButton = new Button[nbOfConfig]; // create an empty array;
        ConstraintSet constraintSet = new ConstraintSet();

        for(int i = 0; i < nbOfConfig;i++){
            int topPadding = (i+1) * 50;
            ConstraintLayout.LayoutParams layoutParamsText = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams. MATCH_PARENT , ConstraintLayout.LayoutParams. MATCH_PARENT) ;
            ConstraintLayout.LayoutParams layoutParamsEdit = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT , ConstraintLayout.LayoutParams.WRAP_CONTENT) ;

            layoutParamsText.setMargins( 10 , (i==0)?(topPadding+30):(topPadding + 95), 0 , 0 );
            layoutParamsEdit.setMargins( 550 , 0, 100 , 0 );

            // create a new textview
            TextView rowTextView = new TextView(this);
            Button button = new Button(this);

            String text = configName + "_"+ i;
            int size = text.split("\\.").length;
            rowTextView.setId(View.generateViewId());
            rowTextView.setText(text.split("\\.")[size-1]);
            rowTextView.setTextSize(20);
            button.setId(View.generateViewId());
            button.setText("Valider");
            button.setTextSize(15);
            button.setWidth(20);
            button.setHeight(10);

            int j = i;
            button.setOnClickListener((v) -> {
                Intent intent = new Intent();
                intent.putExtra("nameConfig",j);
                setResult(2,intent);
                finish();
            });



            constraintSet.clone(constraintLayout);

            constraintLayout.addView(rowTextView, layoutParamsText);
            constraintLayout.addView(button,layoutParamsEdit);
            constraintSet.clone(constraintLayout);

            constraintSet.connect(button.getId(), ConstraintSet.LEFT,rowTextView.getId(),ConstraintSet.LEFT,50);

            constraintSet.connect(button.getId(),ConstraintSet.TOP,constraintLayout.getId(),ConstraintSet.TOP,(i==0)?topPadding:(topPadding + 70));
            constraintSet.applyTo(constraintLayout);

            // add the textview to the linearlayout
            //constraintLayout.addView(rowTextView);

            // save a reference to the textview for later
            myTextViews[i] = rowTextView;
            mySelectButton[i] = button;
        }
    }
}