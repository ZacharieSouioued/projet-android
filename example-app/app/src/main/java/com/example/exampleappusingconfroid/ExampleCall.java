package com.example.exampleappusingconfroid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;

public class ExampleCall extends AppCompatActivity {

    private Context context = this;
    private int nbCall = 0;
    private static final Logger logger = Logger.getLogger(ExampleCall.class.getName());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_call);
        Intent intent = new Intent(this, ExampleCallSaveService.class);
        startService(intent);


        (findViewById(R.id.change_configuration_button)).setOnClickListener((v) -> {
            Intent intentConfig = new Intent(this, newAppValue.class);
            intentConfig.putExtra("nameClass",this.getClass().getName());
            startActivityForResult(intentConfig,1);
        });

        (findViewById(R.id.change_config)).setOnClickListener((v) -> {

            Intent intentConfig = new Intent(this, ExampleCallSaveService.class);
            intentConfig.setAction(ExampleCallSaveService.GET_ALL_CONFIG);
            Bundle bundle = new Bundle();
            bundle.putString("name", this.getClass().getName());
            intentConfig.putExtras(bundle);
            startService(intentConfig);

            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mMessageReceiver, new IntentFilter(this.getClass().getName()));


        });

        //logger.info(Arrays.toString(getDataDir().list()));
        logger.info("files: " + Arrays.toString(getExternalFilesDir(null).list()));
        logger.info("files: " + Arrays.toString(getExternalFilesDirs(null)));
        logger.info("files: " + Arrays.toString(getFilesDir().list())); // the right function to gets saved configurations
        //logger.info(Environment.getStorageDirectory().listFiles());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == 1){
            TextView text1 = findViewById(R.id.textView20);
            TextView text2 = findViewById(R.id.textView21);
            TextView text3 = findViewById(R.id.textView22);
            TextView text4 = findViewById(R.id.textView23);

            assert data != null;
            text1.setText(data.getStringExtra("editext_1"));
            text2.setText(data.getStringExtra("editext_2"));
            text3.setText(data.getStringExtra("editext_3"));
            text4.setText(data.getStringExtra("editext_4"));
        }

        else if(requestCode == 2){
            Intent intentConfig = new Intent(this, ExampleCallSaveService.class);
            intentConfig.setAction(ExampleCallSaveService.SELECT_CONFIG);
            Bundle bundle = new Bundle();
            bundle.putString("name",this.getClass().getName());
            bundle.putInt("nameConfig", data.getIntExtra("nameConfig",0));
            intentConfig.putExtras(bundle);
            startService(intentConfig);
        }

    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            Bundle bundle = intent.getExtras();

            if(bundle.getSerializable("config") == null){
                int nbOfConfig = intent.getIntExtra("nbOfConfig",0);

                Intent intentConfig = new Intent(context, loadAllConfig.class);

                intentConfig.putExtra("nbOfConfig",nbOfConfig);
                intentConfig.putExtra("nameConfig",this.getClass().getName());
                startActivityForResult(intentConfig,2);
            }
            else{
                TextView textv = null;
                HashMap<String, Object> elem = (HashMap<String, Object>)bundle.getSerializable("config");
                elem.remove("_class");


                for(int i = 0; i < elem.size()-1;i++){

                    switch (i){
                        case 0 :
                            textv = (TextView)findViewById(R.id.textView20);
                            textv.setText(elem.get(String.valueOf(i)).toString());
                            break;
                        case 1:
                            textv = (TextView)findViewById(R.id.textView21);
                            textv.setText(elem.get(String.valueOf(i)).toString());
                            break;
                        case 2:
                            textv = (TextView)findViewById(R.id.textView22);
                            textv.setText(elem.get(String.valueOf(i)).toString());
                            break;
                        case 3:
                            textv = (TextView)findViewById(R.id.textView23);
                            textv.setText(elem.get(String.valueOf(i)).toString());
                            break;
                    }
                }
            }


        }
    };

}