package com.example.exampleappusingconfroid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;

public class newAppValue extends AppCompatActivity {
    private static final Logger logger = Logger.getLogger(ExampleCall.class.getName());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        logger.info("ENTER MEN FOU MEC");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_app_value);
        int nbCall = getIntent().getIntExtra("nbCall", 0);

        (findViewById(R.id.save_configuration_button2)).setOnClickListener((v) -> {
            logger.info("files: " + Arrays.toString(getExternalFilesDir(null).list()));
            String nameClass = getIntent().getStringExtra("nameClass");
            basicCall(nameClass);
            Intent intent = new Intent();
            EditText e1 = (EditText)findViewById(R.id.editText_1);
            EditText e2 = (EditText)findViewById(R.id.editText_2);
            EditText e3 = (EditText)findViewById(R.id.editText_3);
            EditText e4 = (EditText)findViewById(R.id.editText_4);

            intent.putExtra("editext_1",e1.getText().toString());
            intent.putExtra("editext_2",e2.getText().toString());
            intent.putExtra("editext_3",e3.getText().toString());
            intent.putExtra("editext_4",e4.getText().toString());
            Toast.makeText(getApplicationContext(), "Config updated", Toast.LENGTH_SHORT).show();
            setResult(1,intent);
            finish();
        });

    }

    void basicCall(String nameClass){
        EditText e1 = (EditText)findViewById(R.id.editText_1);
        EditText e2 = (EditText)findViewById(R.id.editText_2);
        EditText e3 = (EditText)findViewById(R.id.editText_3);
        EditText e4 = (EditText)findViewById(R.id.editText_4);
        logger.info("CLASS : " + ExampleCallSaveService.class);
        Intent intent = new Intent(this, ExampleCallSaveService.class);
        intent.setAction(ExampleCallSaveService.SAVE_ACTION);

        Bundle bundle = new Bundle();
        bundle.putString("tag", "stable");
        bundle.putString("name", nameClass);
        int[] array = new int[4];
        array[0] = Integer.parseInt(e1.getText().toString());
        array[1] = Integer.parseInt(e2.getText().toString());
        array[2] = Integer.parseInt(e3.getText().toString());
        array[3] = Integer.parseInt(e4.getText().toString());


        bundle.putSerializable("content", array);

        intent.putExtras(bundle);
        logger.info("le context nom " + this.toString());

        startService(intent);
    }


}