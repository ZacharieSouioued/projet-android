package com.example.exampleappusingconfroid;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.logging.Logger;

/**
 * SaveService of {@link ExampleCall} activity
 */
public class ExampleCallSaveService extends Service {
	public static String SAVE_ACTION = ExampleCallSaveService.class.getName() + ".save";
	public static String RECEIVE_TOKEN = ExampleCallSaveService.class.getName();
	public static String GET_ALL_CONFIG = ExampleCallSaveService.class.getName() + ".askConfig";
	public static String RECEIVE_ALL_CONFIG = ExampleCallSaveService.class.getName() + ".receiveConfig";
	public static String SELECT_CONFIG = ExampleCallSaveService.class.getName() + ".getConfig";
	public static String RECEIVE_CONFIG = ExampleCallSaveService.class.getName() + ".receiveMyConfig";

	private static String PREFERENCES_NAME = ExampleCall.class.getName();
	private static Logger logger = Logger.getLogger(ExampleCallSaveService.class.getName());

	private int nbOfConfig = 0;
	private SharedPreferences sharedPref;
	private String token;

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private void getToken(){
		new Handler().post(() -> {
			Bundle bundle = new Bundle();
			bundle.putString("name", ExampleCall.class.getName());
			bundle.putString("receiver", ExampleCallSaveService.class.getName());
			Intent intent = new Intent("fr.uge.confroid.receivers.TokenDispenser");
			intent.setPackage("fr.uge.confroid");
			intent.putExtras(bundle);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				startForegroundService(intent);
			} else {
				startService(intent);
			}
			/* */
		});
	}

	private void getAllConfig(Intent intent){


	}

	@Override
	public void onCreate() {
		super.onCreate();
		sharedPref = getApplication().getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
		token = sharedPref.getString("token", null);
		logger.info("token Before " + token);
		if (token == null){
			getToken();
			logger.info("token After " + token);
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		if (intent == null){
			return START_NOT_STICKY;
		}

		if (RECEIVE_TOKEN.equals(intent.getAction())){
			new Handler().post(() -> {
				logger.info("returnToken");
				if (token == null){ // get Token;
					token = intent.getStringExtra("token");
					sharedPref.edit().putString("token", token).apply();
				}
			});
		}
		else if (SAVE_ACTION.equals(intent.getAction())){

			Bundle bundle = intent.getExtras();
				if (bundle == null){
					return START_NOT_STICKY;
				}
				if (token == null){
					getToken();
					return START_STICKY;
				}
				bundle.putString("token", token);
				Intent save = new Intent("fr.uge.confroid.services.ConfigurationPusher.saveConfig");
				save.setPackage("fr.uge.confroid");
				save.putExtras(bundle);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					startForegroundService(save);
				} else {
					startService(save);
				}
		}
		else if(GET_ALL_CONFIG.equals(intent.getAction())){
			logger.info("ENTER PAUSE POTO");

			Bundle bundle = intent.getExtras();

			if (bundle == null){
				return START_NOT_STICKY;
			}

			bundle.putString("token", token);
			Intent save = new Intent("fr.uge.confroid.services.ConfigurationPusher.getAllConfig");
			save.setPackage("fr.uge.confroid");
			save.putExtras(bundle);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				startForegroundService(save);
			} else {
				startService(save);
			}
		}
		else if(RECEIVE_ALL_CONFIG.equals(intent.getAction())){

			Bundle bundle = intent.getExtras();

			if (bundle == null){
				return START_NOT_STICKY;
			}
			nbOfConfig = bundle.getInt("nbConfig");
			Intent newIntent = new Intent(ExampleCall.class.getName());
			newIntent.putExtra("nbOfConfig",nbOfConfig);
			LocalBroadcastManager.getInstance(this).sendBroadcast(newIntent);

		}
		else if(SELECT_CONFIG.equals(intent.getAction())){
			logger.info("NAME je susi passé");
			Bundle bundle = intent.getExtras();

			if (bundle == null){
				return START_NOT_STICKY;
			}


			Intent save = new Intent("fr.uge.confroid.services.ConfigurationPusher.selectConfig");
			save.setPackage("fr.uge.confroid");
			save.putExtras(bundle);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				startForegroundService(save);
			} else {
				startService(save);
			}
		}
		else if(RECEIVE_CONFIG.equals(intent.getAction())){
			Bundle bundle = intent.getExtras();
			logger.info("NAME INFOS : " + bundle.getString("version"));
			logger.info("NAME INFOS : " + bundle.getString("tag"));
			logger.info("NAME INFOS : " + bundle.getSerializable("config"));

			if (bundle == null){
				return START_NOT_STICKY;
			}
			nbOfConfig = bundle.getInt("nbConfig");
			Intent newIntent = new Intent(ExampleCall.class.getName());
			newIntent.putExtra("config",bundle.getSerializable("config"));
			LocalBroadcastManager.getInstance(this).sendBroadcast(newIntent);

		}

		return START_NOT_STICKY;
	}
}
