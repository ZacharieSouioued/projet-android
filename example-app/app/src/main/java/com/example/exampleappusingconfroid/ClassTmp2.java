package com.example.exampleappusingconfroid;

import java.io.Serializable;

/**
 * Exemple of {@link Serializable} class <p>
 * Is Also a field of the class {@link ClassTmp}
 *
 */
public class ClassTmp2 implements Serializable{
	private int x;
	private int y;
	public double publicField;

	public ClassTmp2() {

	}

	public ClassTmp2(int x, int y, double publicField){
		super();
		this.x = x;
		this.y = y;
		this.publicField = 1;
	}
	
	@Override
	public String toString() {
		return "x:" + x + " y:" + y + " publicField:" + publicField;
	}
}
