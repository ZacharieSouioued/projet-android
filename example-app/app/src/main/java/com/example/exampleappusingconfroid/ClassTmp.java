package com.example.exampleappusingconfroid;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Exemple of {@link Serializable} class
 */
public class ClassTmp implements Serializable{
	private int x;
	private int y;
	public double publicField;

	private ClassTmp2[] lala;
	private int[] lala1;

	public ClassTmp() {
		
	}
	
	public ClassTmp(int x, int y, double publicField){
		super();
		this.x = x;
		this.y = y;
		this.publicField = 1;
		lala = new ClassTmp2[3];
		lala[0] = new ClassTmp2(x, y, publicField);
		lala[1] = new ClassTmp2(x, y, publicField);
		lala[2] = new ClassTmp2(x, y, publicField);
		lala1 = new int[3];
		for (int i = 0; i < 3; i++) {
			lala1[i] = i;
		}
	}
	
	@Override
	public String toString() {
		return "x:" + x + " y:" + y + " publicField:" + publicField +
				" lala:" + Arrays.toString(lala) + " lala1:" + Arrays.toString(lala1);
	}
}
